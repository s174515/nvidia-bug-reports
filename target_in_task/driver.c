#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>
#include "gpu_grid.h"
#include "data_transfers.h"

#include "problem_definition.h"

void some_random_kernel(gpuNode * node){
    double * u1 = node->ptrs.u;
    double * u2 = node->ptrs.u_old;
    int size = node->ny*node->nz*node->nx_padded;
    #pragma omp target teams distribute parallel for schedule(static) device(node->id)
    for (int k=0;k<size;k++){
        u1[k] = 0.5*u1[k] + 0.5*u2[k];
    }
}

int main(int argc, char *argv[]) {
    int N;
    double end_tol = 0.0;
    int data_transfer = 0;
    int verbose = 0;
    int version = 0;

    if (argc < 2){
        fprintf(stderr,"Please call:\n\t \"%s <dim> <opt:data_transfer> <opt:kernel><opt:verbose>\"\n",argv[0]);
        fprintf(stderr,"For data transfer select between:\n\t0: nvhpc/22.3 compatible data transfer (default)\n");
        fprintf(stderr,"\t1: nvhpc/22.5 compatible data transfer\n");
        fprintf(stderr,"For kernel exacution select between:\n\t0: Working verion (default)\n");
        fprintf(stderr,"\t1: Buggy tasking version\n");
        fprintf(stderr,"For verbose select between:\n\t0: Don't print domain information (default)");
        fprintf(stderr,"\n\t1: Print domain information and iterations\n");
        return 1;
    }

    N = atoi(argv[1]);

    if (argc >= 2) {
	    data_transfer = atoi(argv[2]);
    }

    if (argc >= 3) {
	    version = atoi(argv[3]);
    }

    if (argc >= 4) {
	    verbose = atoi(argv[4]);
    }

    // Allocate memory
    double* u = (double*) malloc(N*N*N*sizeof(double));

    double* f = (double*) malloc(N*N*N*sizeof(double));

    double* u_old = (double*) malloc(N*N*N*sizeof(double));

    // Initial guesses on u
    init_to_val(u,0.0,N*N*N);
    init_to_val(u_old,0.0,N*N*N);
    init_to_val(f,0.0,N*N*N);

    // Initializing boundary conditions
    double* g_x1 = (double*) malloc(N*N*sizeof(double));
    double* g_xn = (double*) malloc(N*N*sizeof(double));
    double* g_y1 = (double*) malloc(N*N*sizeof(double));
    double* g_yn = (double*) malloc(N*N*sizeof(double));
    init_to_val(g_x1,0.0,N*N);
    init_to_val(g_xn,0.0,N*N);
    init_to_val(g_y1,0.0,N*N);
    init_to_val(g_yn,0.0,N*N);
    
    double time_elapsed = omp_get_wtime();
    
    gpuGrid gpu_grid;
    const int num_devices = omp_get_num_devices();
    const int num_threads = omp_get_max_threads ();
    if (verbose == 1){
        printf("OpenMP enabled with %d threads and %d devices.\n",num_threads,num_devices);
    }
    init_gpu_grid(&gpu_grid,num_devices,N,N,N,verbose);
    if (data_transfer == 0){
        printf("Running nvhpc/22.3 compatible data transfer\n");
        transfer_to_device_22_3(&gpu_grid,&u,&u_old,f,g_x1,g_xn,g_y1,g_yn,verbose);
    }
    else {
        printf("Running nvhpc/22.5 compatible data transfer\n");
        transfer_to_device_22_5(&gpu_grid,&u,&u_old,f,g_x1,g_xn,g_y1,g_yn,verbose);
    }

    // Running some iterations of a random kernel
    int num_iterations = 10000;
    if (version == 0)
    {
        printf("Running version where kernel is wrapped in function.\n");
        #pragma omp parallel
        {
            #pragma omp single nowait
            {
                for(int i=0;i<num_iterations;i++){
                    #pragma omp taskgroup
                    for(int j=0;j<gpu_grid.num_targets;j++){
                        #pragma omp task firstprivate(j) shared(gpu_grid)
                        {
                            gpuNode * node = &gpu_grid.targets[j];
                            some_random_kernel(node);
                        }
                    }
                    printf("\rRan %d of %d iterations",i+1,num_iterations);

                    for(int j=0;j<gpu_grid.num_targets;j++){
                        double * tmp = gpu_grid.targets[j].ptrs.u;
                        gpu_grid.targets[j].ptrs.u = gpu_grid.targets[j].ptrs.u_old;
                        gpu_grid.targets[j].ptrs.u_old = tmp;
                    }
                }
            }
        }
    }
    else {
        printf("Running buggy version where kernel is not wrapped in function.\n");
        #pragma omp parallel
        {
            #pragma omp single nowait
            {
                for(int i=0;i<num_iterations;i++){
                    #pragma omp taskgroup
                    for(int j=0;j<gpu_grid.num_targets;j++){
                        #pragma omp task firstprivate(j) shared(gpu_grid)
                        {
                            gpuNode * node = &gpu_grid.targets[j];
                            double * u1 = node->ptrs.u;
                            double * u2 = node->ptrs.u_old;
                            int size = node->ny*node->nz*node->nx_padded;
                            #pragma omp target teams distribute parallel for schedule(static) device(node->id)
                            for (int k=0;k<size;k++){
                                u1[k] = 0.5*u1[k] + 0.5*u2[k];
                            }
                        }
                    }
                    printf("\rRan %d of %d iterations",i+1,num_iterations);

                    for(int j=0;j<gpu_grid.num_targets;j++){
                        double * tmp = gpu_grid.targets[j].ptrs.u;
                        gpu_grid.targets[j].ptrs.u = gpu_grid.targets[j].ptrs.u_old;
                        gpu_grid.targets[j].ptrs.u_old = tmp;
                    }
                }
            }
        }
    }

    // Stopping times
    time_elapsed = omp_get_wtime()-time_elapsed;

    printf("\nTolerance: %E\nWall time %lf [s]\n", end_tol, time_elapsed);

    // De-allocate memory
    free(gpu_grid.targets);
    free(u);
    free(f);
    free(u_old);
    free(g_x1);
    free(g_xn);
    free(g_y1);
    free(g_yn);
    return 0 ;
}
