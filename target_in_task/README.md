# Bug with `target teams` Clause in Task
This repository contains an example of a bug in the Nvidia HPC SDK. The bug is present in the most recent version of the NVC compiler, release 22.5.

## The Issue
When a kernel is executed in a task, it gives a segmentation fault. However, if the kernel is wrapped in a function, it works. An example of what will give a segmentation fault could be
```C
#pragma omp taskgroup
for(int j=0;j<gpu_grid.num_targets;j++){
    #pragma omp task firstprivate(j) shared(gpu_grid)
    {
        gpuNode * node = &gpu_grid.targets[j];
        #pragma omp target teams distribute parallel for
        ...
    }
}
```
However, it works if one instead does something similar to:
```C
void some_random_kernel(gpuNode * node){
    ...
    #pragma omp target teams distribute parallel for
    ...
}
...
#pragma omp taskgroup
for(int j=0;j<gpu_grid.num_targets;j++){
    #pragma omp task firstprivate(j) shared(gpu_grid)
    {
        gpuNode * node = &gpu_grid.targets[j];
        some_random_kernel(node);
    }
}
```

## Compiling and Running the Code
Type `make` to compile the example. To run a working version of the example, type
```bash
# Use multiple devices
$ export CUDA_VISIBLE_DEVICES=0,1
# It does not make sense to use less threads than devices
$ OMP_NUM_THREADS=2 ./version 400 1 0 1
```
And to run the buggy version, run
```bash
$ export CUDA_VISIBLE_DEVICES=0,1
$ OMP_NUM_THREADS=2 ./version 400 1 1 1
```