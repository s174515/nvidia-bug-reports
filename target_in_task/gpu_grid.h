#ifndef GPU_GRID_LIB
#define GPU_GRID_LIB

#include <stdio.h>
#include <stdlib.h>

typedef struct ptrs_struct{
    double * u;
    double * u_old;
    double * f;
    double * g_x1;
    double * g_xn;
    double * g_y1;
    double * g_yn;
} ptrs_struct;

typedef struct gpuNode {
	int id;
	struct gpuNode * prev;
	struct gpuNode * next;
    int global_x; // minimum global x index;
    int nx;
    int ny;
    int nz;
    int nx_padded;
    ptrs_struct ptrs;
} gpuNode;

typedef struct gpuGrid{
	int num_targets;
	gpuNode * targets;
} gpuGrid;

void init_gpu_grid(gpuGrid * gpu_grid, const int num_devices,const int nx, const int ny, const int nz, const int verbose);

#endif
