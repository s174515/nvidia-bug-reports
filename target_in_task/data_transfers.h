#ifndef DATA_TRANSFERS
#define DATA_TRANSFERS

#include "gpu_grid.h"
#include <omp.h>

void transfer_to_device_22_3(
	gpuGrid * gpu_grid,
    double **u, 
    double **u_old, 
    double *f, 
    double * g_x1,
    double * g_xn, 
    double * g_y1, 
    double * g_yn,
	const int verbose
);

void transfer_to_device_22_5(
	gpuGrid * gpu_grid,
    double **u, 
    double **u_old, 
    double *f, 
    double * g_x1,
    double * g_xn, 
    double * g_y1, 
    double * g_yn,
	const int verbose
);

#endif
