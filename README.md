# NVC 22.3 and 22.5 Bug Reports
During my employment at DTU I have stumbled upon several issues with the OpenMP target offloading support in the Nvidia HCP SDK.

## No Effect of `default(none)`
It was discovered that `default(none)` had no effect on the data scoping for tasks. 

This bug was fixed in the 22.5 release of the NVC compiler.

## `target teams` Region in Task
When embedding a target region in a task a segmentation fault occurs. However, if the target region is wrapped in a function that is called from the task, everything works as expected.
