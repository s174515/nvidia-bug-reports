#include <stdio.h>
#include <math.h>

#define MAX(X, Y) (((X) > (Y)) ? (X) : (Y))

void init_to_val(double *u, double alpha, int n){
	size_t i;
	#pragma omp parallel for schedule(static) firstprivate(alpha)
	for (i=0; i<n; i++){
		u[i] = alpha;
	}
}

