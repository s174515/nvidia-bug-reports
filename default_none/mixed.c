#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>
#include "gpu_grid.h"
#include "data_transfers.h"

#include "problem_definition.h"

int main(int argc, char *argv[]) {
    int N;
    double end_tol = 0.0;
    int version = 0;
    int verbose = 0;

    if (argc < 2){
        fprintf(stderr,"Please call:\n\t \"%s <dim> <opt:version> <opt:verbose>\"\n",argv[0]);
        fprintf(stderr,"For version select between:\n\t0: Working version (default)\n\t1: Failing version\n");
        fprintf(stderr,"For verbose select between:\n\t0: Don't print domain information (default)");
        fprintf(stderr,"\n\t1: Print domain information and iterations\n");
        return 1;
    }

    N = atoi(argv[1]);

    if (argc >= 2) {
	    version = atoi(argv[2]);
    }

    if (argc >= 3) {
	    verbose = atoi(argv[3]);
    }

    // Allocate memory
    double* u = (double*) malloc(N*N*N*sizeof(double));

    double* f = (double*) malloc(N*N*N*sizeof(double));

    double* u_old = (double*) malloc(N*N*N*sizeof(double));

    // Initial guesses on u
    init_to_val(u,0.0,N*N*N);
    init_to_val(u_old,0.0,N*N*N);
    init_to_val(f,0.0,N*N*N);

    // Initializing boundary conditions
    double* g_x1 = (double*) malloc(N*N*sizeof(double));
    double* g_xn = (double*) malloc(N*N*sizeof(double));
    double* g_y1 = (double*) malloc(N*N*sizeof(double));
    double* g_yn = (double*) malloc(N*N*sizeof(double));
    init_to_val(g_x1,0.0,N*N);
    init_to_val(g_xn,0.0,N*N);
    init_to_val(g_y1,0.0,N*N);
    init_to_val(g_yn,0.0,N*N);
    
    double time_elapsed = omp_get_wtime();
    
    gpuGrid gpu_grid;
    const int num_devices = omp_get_num_devices();
    const int num_threads = omp_get_max_threads ();
    if (verbose == 1){
        printf("OpenMP enabled with %d threads and %d devices.\n",num_threads,num_devices);
    }
    init_gpu_grid(&gpu_grid,num_devices,N,N,N,verbose);
    if (version == 0){
        printf("Running stable version\n");
        transfer_to_device(&gpu_grid,&u,&u_old,f,g_x1,g_xn,g_y1,g_yn,verbose);
    }
    else {
        printf("Running unstable version\n");
        transfer_to_device_fail(&gpu_grid,&u,&u_old,f,g_x1,g_xn,g_y1,g_yn,verbose);
    }
    free(gpu_grid.targets);

    // Stopping times
    time_elapsed = omp_get_wtime()-time_elapsed;

    printf("Tolerance: %E\nWall time %lf [s]\n", end_tol, time_elapsed);

    // de-allocate memory
    free(u);
    free(f);
    free(u_old);
    free(g_x1);
    free(g_xn);
    free(g_y1);
    free(g_yn);
    return 0 ;
}
