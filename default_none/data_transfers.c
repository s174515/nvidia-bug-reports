#include "data_transfers.h"

// This version is stable

void transfer_to_device(
	gpuGrid * gpu_grid,
    double **u, 
    double **u_old, 
    double *f, 
    double * g_x1,
    double * g_xn, 
    double * g_y1, 
    double * g_yn,
	const int verbose
){
    const int host = omp_get_initial_device();

	if (verbose == 1){
		printf("\nData transfer from host %d running:\n",host);
	}

    for (int gpu_idx=0;gpu_idx<gpu_grid->num_targets;gpu_idx++){
		gpuNode * node = &(gpu_grid->targets[gpu_idx]);
		int x_start = node->global_x;
		if (node->prev != NULL){
			x_start -= 1;
		}
		ptrs_struct * ptrs = &(node->ptrs);
		const int u_offset = x_start*node->ny*node->nz;
		const int u_length = node->nx_padded*node->ny*node->nz;
		ptrs->u = &((*u)[u_offset]);
		ptrs->u_old = &((*u_old)[u_offset]);
		ptrs->f = &(f[u_offset]);
		ptrs->g_y1 = &(g_y1[x_start*node->nz]);
		ptrs->g_yn = &(g_yn[x_start*node->nz]);
		ptrs->g_x1 = NULL;
		ptrs->g_xn = NULL;
		if (verbose == 1){
			printf("\tMapping u[%d:%d] to device %d.\n",u_offset,u_offset+u_length,node->id);
		}
		// Note that f has a different offset than u and u_old
		//#pragma omp task firstprivate(node,ptrs,u_length)
		{
			if (verbose == 1){
				printf("\tThread %d transferred data to device %d.\n",omp_get_thread_num(),node->id);
			}
			#pragma omp target enter data map(to:ptrs->u[:u_length],ptrs->u_old[:u_length],\
				ptrs->f[:u_length],ptrs->g_y1[:node->nx_padded*node->nz],ptrs->g_yn[:node->nx_padded*node->nz]) device(node->id) nowait
		}
		// Only the first gpu has to deal with the Neumann boundary where x = xmin
		if (node->prev == NULL){
			//#pragma omp task firstprivate(node,g_x1)
			{	
				if (verbose == 1){
					printf("\tThread %d transferred x=xmin boundary to device %d.\n",omp_get_thread_num(),node->id);
				}
				ptrs->g_x1 = g_x1;
				#pragma omp target enter data map(to:ptrs->g_x1[:node->ny*node->nz]) device(node->id) nowait
			}
		}

		// Only the last gpu has to deal with the Neumann boundary where x = xmin
		if (node->next == NULL){
			//#pragma omp task firstprivate(node,g_xn)
			{
				if (verbose == 1){
					printf("\tThread %d transferred x=xmax boundary to device %d.\n",omp_get_thread_num(),node->id);
				}
				ptrs->g_xn = g_xn;
				#pragma omp target enter data map(to:ptrs->g_xn[:node->ny*node->nz]) device(node->id) nowait
			}
		}
	}

	#pragma omp taskwait

    if (verbose == 1){
		printf("\tData transfer finished.\n\n");
	}
}

// This version is unstable

void transfer_to_device_fail(
	gpuGrid * gpu_grid,
    double **u, 
    double **u_old, 
    double *f, 
    double * g_x1,
    double * g_xn, 
    double * g_y1, 
    double * g_yn,
	const int verbose
){
    const int host = omp_get_initial_device();

	if (verbose == 1){
		printf("\nData transfer from host %d running:\n",host);
	}

	// Dividing domain into num_targets targets and mapping data accordingly
	#pragma omp parallel
	{
		// One thread generates all the asynchronous data transfers with OpenMP tasks
		#pragma omp single nowait
		{
			#pragma omp taskgroup
			for (int gpu_idx=0;gpu_idx<gpu_grid->num_targets;gpu_idx++){
				gpuNode * node = &(gpu_grid->targets[gpu_idx]);
				int x_start = node->global_x;
				if (node->prev != NULL){
					x_start -= 1;
				}
				ptrs_struct * ptrs = &(node->ptrs);
				const int u_offset = x_start*node->ny*node->nz;
				const int u_length = node->nx_padded*node->ny*node->nz;
				ptrs->u = &((*u)[u_offset]);
				ptrs->u_old = &((*u_old)[u_offset]);
				ptrs->f = &(f[u_offset]);
				ptrs->g_y1 = &(g_y1[x_start*node->nz]);
				ptrs->g_yn = &(g_yn[x_start*node->nz]);
				ptrs->g_x1 = NULL;
				ptrs->g_xn = NULL;
				if (verbose == 1){
					printf("\tMapping u[%d:%d] to device %d.\n",u_offset,u_offset+u_length,node->id);
				}
				// Note that f has a different offset than u and u_old
				#pragma omp task default(none) firstprivate(node,ptrs,u_length)
				{
					if (verbose == 1){
						printf("\tThread %d transferred data to device %d.\n",omp_get_thread_num(),node->id);
					}
					#pragma omp target enter data map(to:ptrs->u[:u_length],ptrs->u_old[:u_length],\
						ptrs->f[:u_length],ptrs->g_y1[:node->nx_padded*node->nz],ptrs->g_yn[:node->nx_padded*node->nz]) device(node->id)
				}
				// Only the first gpu has to deal with the Neumann boundary where x = xmin
				if (node->prev == NULL){
					// Try to remove for instance verbose from first private
					// You can still compile even though this variable has
					// no data scoping. Very strange
					// #pragma omp task default(none) firstprivate(node,g_x1,ptrs)
					#pragma omp task default(none) firstprivate(node,g_x1,ptrs,verbose)
					{	
						if (verbose == 1){
							printf("\tThread %d transferred x=xmin boundary to device %d.\n",omp_get_thread_num(),node->id);
						}
						ptrs->g_x1 = g_x1;
						#pragma omp target enter data map(to:ptrs->g_x1[:node->ny*node->nz]) device(node->id)
					}
				}

				// Only the last gpu has to deal with the Neumann boundary where x = xmin
				if (node->next == NULL){
					#pragma omp task default(none) firstprivate(node,g_xn,ptrs,verbose)
					{
						if (verbose == 1){
							printf("\tThread %d transferred x=xmax boundary to device %d.\n",omp_get_thread_num(),node->id);
						}
						ptrs->g_xn = g_xn;
						#pragma omp target enter data map(to:ptrs->g_xn[:node->ny*node->nz]) device(node->id)
					}
				}
			}
		}
	}

	#pragma omp taskwait

    if (verbose == 1){
		printf("\tData transfer finished.\n\n");
	}
}
