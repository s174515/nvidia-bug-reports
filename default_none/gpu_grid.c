#include "gpu_grid.h"

void init_gpu_grid(gpuGrid * gpu_grid, const int num_devices,const int nx, const int ny, const int nz, const int verbose){
    gpu_grid->targets = malloc(num_devices*sizeof(gpuNode));
    gpu_grid->num_targets = num_devices;
    const int nx_block = (int) nx/num_devices;
    for(int i = 0;i <num_devices;i++){
        gpu_grid->targets[i].id = i;
        if (i > 0){
            gpu_grid->targets[i].prev = &gpu_grid->targets[i-1];
        }
        else {
            gpu_grid->targets[i].prev = NULL;
        }
        if (i < num_devices - 1){
            gpu_grid->targets[i].next = &gpu_grid->targets[i+1];
        }
        else {
            gpu_grid->targets[i].next = NULL;
        }
        gpu_grid->targets[i].ny = ny;
        gpu_grid->targets[i].nz = nz;

        // Global index of first index of local subdomain
        int global_x = i*nx_block;
        gpu_grid->targets[i].global_x = global_x;

        // Determining the number of local y-z-planes
        int nx_local = (i == num_devices-1) ? nx-i*nx_block : nx_block;
        int nx_local_padded = nx_local;
        if (gpu_grid->targets[i].next != NULL){
            nx_local_padded += 1;
        }
        if (gpu_grid->targets[i].prev != NULL){
            nx_local_padded += 1;
        }
        gpu_grid->targets[i].nx = nx_local;
        gpu_grid->targets[i].nx_padded = nx_local_padded;
        if (verbose == 1){
            printf("GPU %d: \n\t(nx,ny,nz) = (%d,%d,%d)\n\tGlobal index of x: %d\n\tnx with padding: %d\n",i,nx_local,ny,nz,global_x,nx_local_padded);
            printf("\tPrev is null: %d\n\tNext is null: %d\n",(gpu_grid->targets[i].prev == NULL),(gpu_grid->targets[i].next == NULL));
        }
    }
}
