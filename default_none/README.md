# `default(none)` - Data Scoping for Tasks
This bug has been fixed in the 22.5 realease of the Nvidia HPC SDK.
<br>
The issue was that it was possible compile code where the `default(none)` clause had been used
without scoping any of the variables used in a task region.